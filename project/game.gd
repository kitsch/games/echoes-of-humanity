extends Node


signal set_static(value)

var game_ended := false
var last_was_climate := false
var last_was_segment := false

onready var curtain: ColorRect = $Curtain
onready var radio: Radio = $Models/Radio
onready var text: Label = $Text
onready var title_card: Control = $TitleCard
onready var tone: AudioStreamPlayer = $Audio/Tone
onready var tween: RangeTween = $RangeTween
onready var voice: AudioStreamPlayer3D = $Audio/Voice
onready var yarn_player: YarnPlayer = $YarnPlayer


func _ready():
	_on_RangeTween_updated(0.0)
	tween.fade_to(.75)
	yield(tween, "completed")
	yield(get_tree().create_timer(1.0), "timeout")
	title_card.show()
	yield(get_tree().create_timer(5.0), "timeout")
	title_card.hide()
	tween.fade_to(1.0)
	yield(tween, "completed")
#	_on_RangeTween_updated(1.0)
	yarn_player.play("Gameplay")


func _process(delta):
	$PopIns/Turbine/Spinners.rotation.x -= delta * .111
	tone.volume_db = lerp(tone.volume_db, -50 if radio.is_playing else -25, delta * 40)

	var x = clamp(abs(radio.freq - radio.goal) - radio.DEADZONE, 0, .1)
	tone.pitch_scale = 1 + (.1 - x) * 5 + randf() * .01
	if x == 0:
		tone.pitch_scale += .25


func _on_YarnPlayer_command_triggered(command, arguments):
	var args = Array(arguments)

	match command:
		"segment":
			if not last_was_segment:
				yield(get_tree().create_timer(1.5), "timeout")

			if radio.lit_leds == 3:
				radio.reset()

			radio.is_playing = false
			var path = "assets/audio/tranmissions/%s.wav" % args.pop_front()
			var stream: AudioStream = load(path)
			voice.stream = stream
			yield(radio, "lit")
			voice.play(float(args.pop_front()))
			text.text = PoolStringArray(args).join(" ")
			yield(get_tree().create_timer(.4), "timeout")
			voice.stop()
			text.text = ""
			yield(get_tree().create_timer(.2), "timeout")
			last_was_segment = true
			yarn_player.do_next()

		"show_model":
			yield(get_tree().create_timer(1.0), "timeout")
			$StaticTween.value = 0.0
			$StaticTween.fade_to(1.0)
			yield($StaticTween, "completed")
			_on_StaticTween_updated(0.0)
			$PopIns.get_node(arguments[0]).show()
			yarn_player.do_next()


func _on_YarnPlayer_dialogue_triggered(speaker, t):
	radio.is_playing = true
	var chev = t.find("<")
	var file: String = t.substr(chev + 1, t.length() - chev -2)
	var path = "assets/audio/tranmissions/%s.wav" % file
	var stream: AudioStream = load(path)
	voice.stream = stream
	if last_was_segment or (file.begins_with("climate") and not last_was_climate):
		yield(get_tree().create_timer(1), "timeout")
	text.text = t.substr(0, chev - 1)
	voice.play()
	yield(voice, "finished")
	text.text = ""
	last_was_segment = false
	last_was_climate = file.begins_with("climate")
	yarn_player.do_next()


func _on_RangeTween_updated(value):
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Master"), linear2db(value))
	curtain.modulate.a = (1.0 - value)


func _on_YarnPlayer_playback_ended():
	$HandTween.fade_to(1.35)
	$StaticTween.value = 0.0
	$StaticTween.fade_to(1.0)
	game_ended = true
	yield($HandTween, "completed")
	_on_StaticTween_updated(0.0)
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Master"), linear2db(.5))
	tone.stop()
	curtain.modulate.a = 1.0
	$CreditsMenu.play()


func _on_HandTween_updated(value):
	$Spatial/Hand.transform.origin.z = value


func _on_StaticTween_updated(value):
	emit_signal("set_static", value * .01 if game_ended else value)
	$Audio/Static.volume_db = -25 + value * 25

