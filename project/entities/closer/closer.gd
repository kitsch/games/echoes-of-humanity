class_name Closer
extends VBoxContainer


onready var bar: ProgressBar = $ProgressBar


func _ready():
	visible = false


func _physics_process(delta):
	visible = Input.is_action_pressed("pause")

	if not visible:
		return

	if Input.is_action_just_pressed("pause"):
		bar.value = 0.0

	bar.value += delta * .5

	if bar.value >= 1.0:
		get_tree().quit()
