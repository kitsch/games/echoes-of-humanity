class_name Radio
extends Spatial

signal lit

const DEADZONE := .0333
const TIME_TO_HOLD := 1.332

var freq := 0.5
var goal: float = 0.0
var is_playing := true
var time_since_move := 0.0

onready var dial: MeshInstance = $Mesh/Dial
onready var inactive_led_mat: Material = preload("./inactive_led.material")
onready var leds = [
	$Mesh/Sphere,
	$Mesh/Sphere001,
	$Mesh/Sphere002
]
onready var lit_leds := 0


func _ready():
	reset()


func _physics_process(delta):
	if is_playing:
		return

	dial.rotation_degrees.z = (freq - .5) * 135 * 2

	var intent = Input.get_action_strength("move_right") - Input.get_action_strength("move_left")
	freq = clamp(freq + intent * delta * .666, 0.0, 1.0)

	# Track how long the dial hasn't been moved for
	if intent == 0:
		time_since_move += delta
	else:
		time_since_move = 0.0

	if time_since_move >= TIME_TO_HOLD and abs(freq - goal) < DEADZONE:
		light_next()

func light_next():
	leds[lit_leds].set_material_override(null)
	lit_leds += 1
	is_playing = true
	setup_freq()
	emit_signal("lit")

func reset():
	setup_freq()
	lit_leds = 0
	for led in leds:
		led.set_material_override(inactive_led_mat)

func setup_freq():
	goal = freq
	while abs(goal - freq) < DEADZONE:
		goal = randf()
