class_name Splash
extends Control
# Splash to fade in and out a logo.


signal completed

const WAIT_TIME := 1.5

onready var tween: RangeTween = $RangeTween


func _ready():
	_set_alpha(0.0)


func play():
	tween.fade_to(1.0)
	yield(tween, "completed")

	yield(get_tree().create_timer(WAIT_TIME), "timeout")

	tween.fade_to(0.0)
	yield(tween, "completed")

	emit_signal("completed")


func _set_alpha(value: float):
	modulate.a = value
