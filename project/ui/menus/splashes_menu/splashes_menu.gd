class_name SplashesMenu
extends Control
# Displays the splashes at the start of the game sequentially.


onready var godot: Splash = $GodotSplash
onready var kitsch: Splash = $KitschSplash


func _ready():
	kitsch.play()


func _on_KitschSplash_completed():
	godot.play()


func _on_GodotSplash_completed():
	get_tree().change_scene("res://game_wrapped.tscn")
