extends Control


var index := 0


func play():
	get_child(index).show()
	yield(get_tree().create_timer(5.0), "timeout")

	if get_children().size() == index + 1:
		return

	get_child(index).hide()
	index += 1

	play()
